import React from 'react';
import { Link } from 'react-router-dom';
import './dashboard.css';
import { Animated } from "react-animated-css";

class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showMenu: false,
            showHeaderFooter: localStorage.getItem('isLoggedIn')
        }
        console.log(this.state)
    }
    showMenu = () => {
        return this.props.children.filter((menu, index) => {
            return index > 0
        })
    }
    menuBarToggle = () => {
        this.setState({
            showMenu: !this.state.showMenu
        });
    }
    logout = () => {
        localStorage.removeItem('isLoggedIn');
        // this.props.history.push('/')
        console.log(this.props)
    }
    render() {
        return (<React.Fragment>
            {
                this.state.showHeaderFooter === 'true' ? (
                    <div className="header">
                        <img alt='logo' className='logo' src="https://www.neosofttech.com/sites/all/themes/neosoft2017/images/neosoft.svg">
                        </img>
                        <span title='Menu List' onClick={this.menuBarToggle}>
                            <i className="fa fa-th fa-lg" aria-hidden="true"></i>
                        </span>
                        <div className="header-right">
                            {/* <a href="#" onClick={this.logout}>Logout</a> */}
                            <span>
                                Logout
                            </span>
                        </div>
                    </div>
                ) : null
            }
            {
                this.state.showMenu ? (
                    <Animated animationIn="bounceInLeft" animationOut="fadeOut" isVisible={true}>
                        <div className="menu">
                            {
                                this.showMenu().map(menu => (
                                    <Link to={menu.props.path}>
                                        <i className="fa fa-book" aria-hidden="true" style={{ marginRight: '5px' }}></i>{menu.props.name}
                                    </Link>
                                ))
                            }
                        </div>
                    </Animated>
                ) : null
            }
            <div>
                {this.showMenu()}
            </div>
            {
                this.state.showHeaderFooter === 'true' ? (
                    <div className="footer">
                        <p style={{ marginLeft: "2%" }}>Created With &nbsp;
                    <i className="fa fa-heart" aria-hidden="true" style={{ color: 'red' }}></i> by NeoSoft Technologies<i className="fa fa-copyright" aria-hidden="true"></i></p>
                    </div>
                ) : null
            }
        </React.Fragment>);
    }
}

export default Dashboard;