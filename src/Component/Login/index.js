import React from 'react';
import './login.css';


class Login extends React.Component {
    constructor() {
        super();
        this.cardRef = React.createRef();
        this.state = {
            signUp: false,
            username: '',
            password: '',
            confirmPassword: '',
            role: '',
            usernameFlag: false,
            passwordFlag: false,
            roleFlag: false
        }
    }
    componentDidMount() {

    }
    switchToSignUp = () => {
        this.setState({
            signUp: !this.state.signUp
        });
        if (!this.state.signUp) {
            this.cardRef.current.style.height = "52vh"
        } else {
            this.cardRef.current.style.height = "37vh"
        }
    }
    login = () => {
        if (!this.ValidateEmail(this.state.username)) {
            this.setState({
                usernameFlag: true
            });
        } else {
            this.setState({
                usernameFlag: false
            });
        }
        if (this.state.password === "") {
            this.setState({
                passwordFlag: true
            })
        } else {
            this.setState({
                passwordFlag: false
            })
        }

        if ((this.state.username !== "") && (this.state.passwordFlag !== "")) {
            console.log('all ok')
        }
        this.props.history.push('/dashboard');
    }
    signUp = () => {

    }
    validateUsername = (parameter) => {
        this.setState({
            username: parameter.target.value
        });
    }
    ValidateEmail(mail) {
        if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
            return (true)
        }
        return (false)
    }
    ValidateMultipleSpaces(password) {
        if (/  +/g.test(password)) {
            return (true)
        }
        return (false)
    }
    checkPassword = (parameter) => {
        this.setState({
            password: parameter.target.value
        });
    }
    render() {
        return (
            <React.Fragment>
                <div className="bg">
                    <div className="vertical-center">
                        <div className="card" ref={this.cardRef}>
                            {!this.state.signUp ? (
                                <div className="container">
                                    <h4>Login</h4>
                                    <div className="input-container">
                                        <i className="fa fa-user icon"></i>
                                        <input className="input-field" type="email" placeholder="Email" name="usrnm"
                                            onChange={this.validateUsername}
                                        />
                                        {this.state.usernameFlag ? (<i className="fa fa-exclamation-triangle warningIcon" title='Invalid Email Id'></i>) : null}
                                    </div>
                                    <div className="input-container">
                                        <i className="fa fa-key icon"></i>
                                        <input className="input-field" type="password" placeholder="Password" name="psw"
                                            onChange={this.checkPassword}
                                        />
                                        {this.state.passwordFlag ? (<i className="fa fa-exclamation-triangle warningIcon" title="Spaces Not Allowed in Password"></i>) : null}
                                    </div>
                                    <div className='btnDiv'>
                                        <button type="button" className="loginBtn" onClick={this.login} >Login</button>
                                        <button type="button" className="signUpBtn" onClick={this.switchToSignUp}>Sign up</button>
                                    </div>
                                </div>
                            ) : (
                                    <div className="container">
                                        <h4>Sign Up</h4>
                                        <div className="input-container">
                                            <i className="fa fa-user icon"></i>
                                            <input className="input-field" type="text" placeholder="Username" name="usrnm" />
                                        </div>
                                        <div className="input-container">
                                            <i className="fa fa-key icon"></i>
                                            <input className="input-field" type="password" placeholder="Password" name="psw" />
                                        </div>
                                        <div className="input-container">
                                            <i className="fa fa-key icon"></i>
                                            <input className="input-field" type="password" placeholder="Confirm Password" name="psw" />
                                        </div>
                                        <div>
                                            <select className='selectBox' placeholder="Role">
                                                <option value="-1">Select Role</option>
                                                <option value="1">Network Admin</option>
                                                <option value="2">Dev</option>
                                            </select>
                                        </div>
                                        <div className='btnDiv'>
                                            <button type="submit" className="signUpBtn" >Submit</button>
                                            <button type="submit" className="cancel" onClick={this.switchToSignUp}>Cancel</button>
                                        </div>
                                    </div>
                                )}
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default Login;