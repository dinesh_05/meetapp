import React from 'react';
import './header.css';
class Header extends React.Component {
    logout = () => {
        localStorage.removeItem('isLoggedIn');
        // this.props.history.push('/')
        console.log(this.props)
    }
    render() {
        return (
            <React.Fragment>
                <div className="header">
                    <img alt='logo' className='logo' src="https://www.neosofttech.com/sites/all/themes/neosoft2017/images/neosoft.svg">
                    </img>
                    {/* <span title='Menu List' onClick={this.menuBarToggle}> */}
                    <span title='Menu List' >
                        <i class="fa fa-th fa-lg" aria-hidden="true"></i>
                    </span>
                    <div className="header-right">
                        <a href="#" onClick={this.logout}>Logout</a>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Header