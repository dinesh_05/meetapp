import React from 'react';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import Login from "../Login";
import Dashboard from '../dashboard';
import RoomBook from '../dashboard/roombook';
import DeviceBook from '../dashboard/devicebook';

class App extends React.Component {
    constructor(props) {
        super();
    }
    render() {
        return (<React.Fragment>
            <BrowserRouter>
                <Switch>
                    <Route
                        path='/'
                        component={Login}
                        exact
                    >
                    </Route>
                    < Dashboard >
                        <Route
                            exact
                            path='/dashboard'
                            component={Dashboard}
                            key={'dash'}
                            name="Dashboard"
                        >
                        </Route>
                        <Route
                            exact
                            path='/dashboard/roombook'
                            component={RoomBook}
                            key={'roombook'}
                            name="Meeting Hall Booking"
                        >
                        </Route>
                        <Route
                            exact
                            path='/dashboard/devicebook'
                            component={DeviceBook}
                            key={'device'}
                            name="Device Booking"
                        >
                        </Route>
                    </Dashboard>
                </Switch>
            </BrowserRouter>
        </React.Fragment >)
    }
}

export default App;